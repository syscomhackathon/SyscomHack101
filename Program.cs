﻿using System;
using System.Threading;

namespace SyscomHack101
{
    class Program
    {
        static string[,] board;
        static void Main(string[] args)
        {
            bool kjør = true;
            while(kjør){      
                Console.WriteLine("Hva vil du kjøre?");
                Console.WriteLine("1. TripTrapTresko");
                Console.WriteLine("2. Navn");
                Console.WriteLine("3. Alder");            
                Console.WriteLine("4. Oddetall");
                Console.WriteLine("5. Avslutt");            
                string tall = Console.ReadLine();

                Console.Clear();

                if(tall == "1"){
                    TripTrapTresko();
                }
                else if(tall == "2"){
                    Navn();
                }
                else if(tall == "3"){
                    Årstall();
                }
                else if(tall == "4"){
                    Teller();
                }
                else if(tall == "4"){
                    kjør = false; //Bryt ut av while loopen
                }
                else{
                    Console.WriteLine("Du må velge et tall mellom 1 og 5 og trykke enter.");
                }
                Console.Clear();
            }
            //Her avslutter programmet
        }

        static void TripTrapTresko(){
            //Initialisere brettet
            board = new string[,] {{" "," "," "},{" "," "," "},{" "," "," "}};

            //Start spill loopen.
            while(IngenVinner()){
                //Tegn brettet
                TegnBrett();
                Console.WriteLine("Hvor vil du sette brikken din? Tast rad,kolonne og avslutt med enter.");
                //Ta imot spiller input
                TaImotSpillerInput();
            }
        }

        static void TaImotSpillerInput(){
            Console.WriteLine("Hvor vil du spille?");
            string input = Console.ReadLine();
            if(!input.Contains(",")){
                Console.WriteLine("Du må angi et sted i brette på formatet rad,kolonne");
                TaImotSpillerInput();
                return;
            }
            // hvis spiller skriver 1,2 vil split funksjonen gi en liste {"1","2"}
            // når vi splitter på komma
            string[] splitResultat = input.Split(",");
            string radString = splitResultat[0];
            string kolString = splitResultat[1];


            // deretter må vi konvertere de to verdiene fra tekst til tall
            
            int rad = -1;
            int kol = -1;

            // TryParse funksjonen forsøker å konvertere teksten til tall og gir tilbake en true/false (bool) verdi som signaliserer
            // om konverteringen var vellykket. Tallverdien blir skrevet til variabelen markert med out.
            bool radSuccess = int.TryParse(radString, out rad);
            bool kolSuccess = int.TryParse(kolString, out kol);


            // Deretter kan vi feilsjekke for å verifisere at spilleren har fulgt reglene
            // Tegnet || betyr ELLER, så hvis radSuccess er false (legg merke til utropstegn)
            // ELLER rad er større enn 3
            // ELLER rad er mindre enn 0
            // SÅ kjør innholdet i IF { }
            if(!radSuccess || rad > 3 || rad < 0){
                Console.WriteLine("Radnummer må være et tall fra 0 til 3");
                
                //Hvis spilleren har tastet en rad som er ugyldig kjører vi funksjonen på nytt
                TaImotSpillerInput();
                return;
            }
            if(!kolSuccess || kol > 3 || kol < 0){
                Console.WriteLine("Kolonnenummer må være et tall fra 0 til 3");

                //Hvis spilleren har tastet en kolonne som er ugyldig kjører vi funksjonen på nytt                
                TaImotSpillerInput();
                return;
            }
            
            SettFelt(rad,kol,"o");
        }

        static bool IngenVinner(){
            return true;
        }
        

        static void SettFelt(int rad, int kol, string verdi){
            board[rad,kol] = verdi;
        }

        static void TegnBrett(){
            Console.Clear();
            for(int rad = 0;rad < 3; rad++){
                for(int kol=0;kol < 3; kol++){
                    Console.Write("|"+board[rad,kol]);
                }
                Console.WriteLine("|");
            }
        }

        static bool ErOddetall(int tall)
        {
            int resultat = tall % 2;
            return resultat == 1;
        }
        static void Lister(){
            int[] tall = new int[] {1,2,3,4,5};

            int[] tall2 = new int[3];

            string[] tekst = new string[] {"test", "bip", "bop"};

            for(int i = 0;i < tall.Length;i++){
                Console.WriteLine("Liste plass "+i+" er "+tall[i]);
            }
        }

        static void Navn(){
            Console.WriteLine("Hva heter du?");

            string navn = Console.ReadLine();

            Console.WriteLine("Hei "+navn+"!");
            
            Console.WriteLine("Trykk enter for å fortsette");            
            
            Console.ReadLine();
        }

        static void Teller(){
            int teller = 0;
            while(teller < 10){
                teller++;
                Thread.Sleep(1000);

                if(ErOddetall(teller)){
                    Console.WriteLine(teller+ " (Oddetall)");
                }
                else{
                    Console.WriteLine(teller+ " (Partall)");
                }
            }

            Console.WriteLine("Trykk enter for å fortsette");            
            
            Console.ReadLine();
        }
        static void Årstall()
        {
            Console.WriteLine("Hvor gammel er du?");
            string alder = Console.ReadLine();

            int årstall = 2017-int.Parse(alder);

            if(årstall < 1985)
            {
               Console.WriteLine("Du er født før Asbjørn"); 
            }
            else if(årstall == 1985)
            {
                Console.WriteLine("Du er født samme år som Asbjørn");                
            }
            else
            {
                Console.WriteLine("Du er født etter Asbjørn");
            }

            Console.WriteLine("Du er født i "+årstall);

            Console.WriteLine("Trykk enter for å fortsette");            
            
            Console.ReadLine();
        }
    }
}
